//
//  ViewController.swift
//  swiftcalc
//
//  Created by Click Labs on 1/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var firstEntry:Double? = 0
    
    var secondEntry:Double? = 0
    
    var isTypingNumbers:Bool = false
   
    var op:String = ""
    
    var result:Double = 0
    
    var flag = 0
    
    var isOperation = false
    var isDot = 0
    
    @IBOutlet weak var viewLabel: UILabel!
  
   

    @IBAction func numberButton(sender: AnyObject) {
        var num = sender.currentTitle!!
    
    
        
        if isTypingNumbers {
            if num == "." && isDot == 1 {
            return
            }
            else {
            viewLabel.text = viewLabel.text! + num
            }
            }
        else {
            viewLabel.text = num
            isTypingNumbers = true
        }
        isOperation = false
        if num == "." {
            isDot = 1
    
        
        
    }
    }
    
    
    @IBAction func calculationTapped(sender: AnyObject) {
        if isOperation == false {
        if flag == 0  {
        var str = viewLabel.text!
            
            
            
            isTypingNumbers = false      //this code sends changes value of operation
            
            firstEntry =  (str as NSString).doubleValue //stores value of firstnumber
            
            op = sender.currentTitle!! // sends current title into the operation thus copied into every operator
            
            
            
            viewLabel.text = ""
         flag++
        }
        else {
           
         result(isTypingNumbers)
           // viewLabel.text=""

            firstEntry = result
            isTypingNumbers = false      //this code sends changes value of operation
            
            op = sender.currentTitle!!   // sends current title into the operation thus copied into every operator
            
            
            flag++
            
            
            
            
        }
        
        }
        
        
        
        
        
        isDot = 0
    isOperation = true
        
     op = sender.currentTitle!!
        
        
    }
    @IBAction func result(sender: AnyObject) {
        
        
        
        isTypingNumbers = false    //this code represent what equal does
        
        
        
        
        
        var newstr = viewLabel.text!
        
        
        
        secondEntry = (newstr as NSString).doubleValue
        
        //viewLabel.text = ""
        
        if op == "+" {
            
            
            
            result = firstEntry! + secondEntry!
            
            
            
            viewLabel.text = "\(Double(round(1000*result)/1000))"
            
            
            
        } else if op == "-" {
            
            
            result = firstEntry! - secondEntry!
            
            
            
            viewLabel.text = "\(Double(round(1000*result)/1000))"
            
        }
            
            
            
        else if op == "*" {
            
            
            result = firstEntry! * secondEntry!
            
            
            
            viewLabel.text = "\(Double(round(1000*result)/1000))"
            
        }
            
            
            
        else if op == "/" {
            
            
            
            if secondEntry != 0
                
                
                
            {
                
                
                result = firstEntry! / secondEntry!
                
               // Double(round(1000*result)/1000)
                
                viewLabel.text = "\(Double(round(1000*result)/1000))"
                
                
                
            }else{
                
                
                
                viewLabel.text = "DIV BY 0"
                
                
                
            }
            
            
            
            
            
            
            
        }
        
        op = ""
        isOperation = false

        
    }
    

    
    @IBAction func eraseButton(sender: AnyObject) {
         self.clear()
    }
    
    func clear() {
        flag = 0
        firstEntry = 0
        secondEntry = 0
       result = 0
        isDot = 0
        viewLabel.text = ""
        isOperation = false
     
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         viewLabel.adjustsFontSizeToFitWidth = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

